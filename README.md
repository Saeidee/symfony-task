symfony-task
==============


# Installation

First, clone this repository:

```bash
$ git clone https://Saeidee@bitbucket.org/Saeidee/symfony-task.git
```

Add `symfony.localhost` in your `/etc/hosts` file.

Then, run:

```bash
$ docker-compose up
```

You are done, you can visit your Symfony application on the following URL: `http://symfony.localhost` (and access Kibana on `http://symfony.localhost:81`)

if you could't see the page just go to (start from here) section!

_Note :_ you can rebuild all Docker images by running:

```bash
$ docker-compose build
```

# How it works?

Here are the `docker-compose` built images:

* `db`: This is the MySQL database container (can be changed to postgresql or whatever in `docker-compose.yml` file),
* `php`: This is the PHP-FPM container including the application volume mounted on,
* `nginx`: This is the Nginx webserver container in which php volumes are mounted too,
* `elk`: This is a ELK stack container which uses Logstash to collect logs, send them into Elasticsearch and visualize them with Kibana.

This results in the following running containers:

```bash
> $ docker-compose ps
        Name                       Command               State              Ports
--------------------------------------------------------------------------------------------
dockersymfony_db_1      docker-entrypoint.sh mysqld      Up      0.0.0.0:3306->3306/tcp
dockersymfony_elk_1     /usr/bin/supervisord -n -c ...   Up      0.0.0.0:81->80/tcp
dockersymfony_nginx_1   nginx                            Up      443/tcp, 0.0.0.0:80->80/tcp
dockersymfony_php_1     php-fpm7 -F                      Up      0.0.0.0:9000->9000/tcp
```

# Start from here!
Plase change the permission of logs/, symfony/var, symfony/var/cache, symfony/var/logs, symfony/var/session!
First go to your php container and then change the folders permission!

```bash 
$ docker exec -it <php-container-id> bash
$ chmod -R 777 <folders>
```
Note: if bash not worked just use sh like below:
```bash 
$ docker exec -it <php-container-id> sh
```

You can access Nginx and Symfony application logs in the following directories on your host machine:

* `logs/nginx`
* `logs/symfony`

Now it is time to install the package dependencies and we will install it by composer!
just write the below command in your container:

```bash 
$ coposer install
```

Make sure you adjust `database_host` in `parameters.yml` to the database container alias "db"
as well as database = symfony, user = symfony and password = symfony!
port should be same: 3306

and after that migrate!

```bash 
$ php bin/console doctrine:migrations:migrate
```

After that open `http://symfony.localhost/currency` and you will see the currency page!

if you see there is no rate just try to run the below command:

```bash 
$ php bin/console api:fetch-currencies
```

and check the page again!
# Use Kibana!

You can also use Kibana to visualize Nginx & Symfony logs by visiting `http://symfony.localhost:81`.

_Note :_ if you faced any problem duraing the installition just call **05313640559** or mail me **sk.saeidee@yahoo.com** **:)** 