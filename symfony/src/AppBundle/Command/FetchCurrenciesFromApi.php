<?php
namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use AppBundle\Utils\Api\Api1\ApiAdapter as Api1Adapter;
use AppBundle\Utils\Api\Api2\ApiAdapter as Api2Adapter;
use AppBundle\Utils\Api\Api1\Api as Api1;
use AppBundle\Utils\Api\Api2\Api as Api2;
use AppBundle\Entity\Currency;

class FetchCurrenciesFromApi extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('api:fetch-currencies')
            ->setDescription('Fetch currencies from API.')
            ->setHelp('This command allows you to fetch currencies from all of Api providers...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $dollar = [];
        $euro = [];
        $pound = [];

        $output->writeln([
            'Fetching Currencies From API',
            '============================',
            '',
        ]);

        // Making the object of first API and using it with it's Adapter 
        $api1 = new Api1Adapter(new Api1());

        // for all currency API even new API we are just calling requestCurrencies!
        $api1->requestCurrencies();
        
        // Pushing currency rates to their specific array
        array_push($dollar, $api1->getDollar());
        array_push($euro, $api1->getEuro());
        array_push($pound, $api1->getPound());
        
        // Making the object of second API and using it with it's Adapter 
        $api2 = new Api2Adapter(new Api2());
        
        // for all currency API even new API we are just calling requestCurrencies!
        $api2->requestCurrencies(); 
        
        // Pushing currency rates to their specific array
        array_push($dollar, $api2->getDollar());
        array_push($euro, $api2->getEuro());
        array_push($pound, $api2->getPound());

        $entityManager = $this->getContainer()->get('doctrine')->getEntityManager();
        
        // Saving lowest rate of currencies to database
        $currency = new Currency();
        $currency
            ->setDollar(min($dollar))
            ->setEuro(min($euro))
            ->setPound(min($pound));

        $entityManager->persist($currency);
        $entityManager->flush();

        $output->writeln('Finished!');
    }
}
?>