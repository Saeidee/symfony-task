<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Currency;

class CurrencyController extends Controller
{
    /**
     * @Route("/currency", name="currency_index")
     */
    public function indexAction()
    {   
        // Getting the latest currency record from database
        $repository = $this->getDoctrine()->getRepository(Currency::class);
        $currencies = $repository->findBy([], ['id' => 'DESC'], 1);
        return $this->render('@App/Currency/index.html.twig', array(
            'currencies' => reset($currencies)
        ));
    }

}
