<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Currency
 *
 * @ORM\Table(name="currency")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CurrencyRepository")
 */
class Currency
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="dollar", type="float")
     */
    private $dollar;

    /**
     * @var float
     *
     * @ORM\Column(name="euro", type="float")
     */
    private $euro;

    /**
     * @var float
     *
     * @ORM\Column(name="pound", type="float")
     */
    private $pound;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dollar
     *
     * @param float $dollar
     *
     * @return Currency
     */
    public function setDollar($dollar)
    {
        $this->dollar = $dollar;

        return $this;
    }

    /**
     * Get dollar
     *
     * @return float
     */
    public function getDollar()
    {
        return $this->dollar;
    }

    /**
     * Set euro
     *
     * @param float $euro
     *
     * @return Currency
     */
    public function setEuro($euro)
    {
        $this->euro = $euro;

        return $this;
    }

    /**
     * Get euro
     *
     * @return float
     */
    public function getEuro()
    {
        return $this->euro;
    }

    /**
     * Set pound
     *
     * @param float $pound
     *
     * @return Currency
     */
    public function setPound($pound)
    {
        $this->pound = $pound;

        return $this;
    }

    /**
     * Get pound
     *
     * @return float
     */
    public function getPound()
    {
        return $this->pound;
    }
}

