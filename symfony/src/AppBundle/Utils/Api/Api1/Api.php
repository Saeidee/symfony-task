<?php
namespace AppBundle\Utils\Api\Api1;
use GuzzleHttp\Client as Client;

class Api {
    
    /**
     * @constructor
     */
    public function __construct() {}
    
    /**
     * @return json
     */
    public function getCurrenciesWithEnglishAlphabetic() {
        $client = new Client();
        return $client->request('GET', 'http://www.mocky.io/v2/5a74519d2d0000430bfe0fa0')->getBody();
    }
} 
?>