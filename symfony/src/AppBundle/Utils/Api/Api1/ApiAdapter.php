<?php
namespace AppBundle\Utils\Api\Api1;
use AppBundle\Utils\Adapters\CurrencyApiAdapter;
use AppBundle\Utils\Api\Api1\Api;

class ApiAdapter implements CurrencyApiAdapter {
    
    private $api;
    private $currencies;
    
    /**
     * @constructor
     */
    public function __construct(Api $api) {
        $this->api = $api;
    }

    /**
     * @return null
     */
    public function requestCurrencies() {
        $this->currencies = json_decode($this->api->getCurrenciesWithEnglishAlphabetic())->result;
        return $this->currencies;
    }

    /**
     * @return float
     */
    public function getDollar() {
        return $this->filter('USDTRY');
    }

    /**
     * @return float
     */
    public function getEuro() {
        return $this->filter('EURTRY');
    }

    /**
     * @return float
     */
    public function getPound() {
        return $this->filter('GBPTRY');
    }

    /**
     * @return float
     */
    public function filter($symbol) {
        $currency = array_filter($this->currencies, function ($value, $key) use ($symbol) {
            return $value->symbol == $symbol;
        }, ARRAY_FILTER_USE_BOTH);
        return count($currency) > 0 ? reset($currency)->amount : 0.000; 
    }
}
?>