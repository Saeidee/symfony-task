<?php
namespace AppBundle\Utils\Api\Api2;
use GuzzleHttp\Client as Client;

class Api {

    /**
     * @constructor
     */
    public function __construct() {}
    
    /**
     * @return json
     */
    public function getCurrenciesWithTurkishAlphabetic() {
        $client = new Client();
        return $client->request('GET', 'http://www.mocky.io/v2/5a74524e2d0000430bfe0fa3')->getBody();
    }
} 
?>