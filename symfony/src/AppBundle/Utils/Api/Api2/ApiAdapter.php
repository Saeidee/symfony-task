<?php
namespace AppBundle\Utils\Api\Api2;
use AppBundle\Utils\Adapters\CurrencyApiAdapter;
use AppBundle\Utils\Api\Api2\Api;

class ApiAdapter implements CurrencyApiAdapter {

    private $api;
    private $currencies;
    
    /**
     * @constructor
     */
    public function __construct(Api $api) {
        $this->api = $api;
    }
     
    /**
     * @return null
     */
    public function requestCurrencies() {
        $this->currencies = json_decode($this->api->getCurrenciesWithTurkishAlphabetic());
        return $this->currencies;
    }

    /**
     * @return float
     */
    public function getDollar() {
        return $this->filter('DOLAR');
    }

    /**
     * @return float
     */
    public function getEuro() {
        return $this->filter('AVRO');
    }

    /**
     * @return float
     */
    public function getPound() {
        return $this->filter('İNGİLİZ STERLİNİ');
    }

    /**
     * @return float
     */
    public function filter($symbol) {
        $currency = array_filter($this->currencies, function ($value, $key) use ($symbol) {
            return $value->kod == $symbol;
        }, ARRAY_FILTER_USE_BOTH);
        return count($currency) > 0 ? reset($currency)->oran : 0.000; 
    }
}
?>