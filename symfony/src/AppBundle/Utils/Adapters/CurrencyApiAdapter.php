<?php
namespace AppBundle\Utils\Adapters;
/**
 * This is Currency API Adapter Interface to give us a unique structure for all currency API.
 * we can just implement this Adapter with any currency API which we want to make. 
 */
interface CurrencyApiAdapter {
    /**
     * @return array
     */
    public function requestCurrencies();

    /**
     * @return float
     */
    public function getDollar();

    /**
     * @return float
     */
    public function getEuro();

    /**
     * @return float
     */
    public function getPound();

}

?>